#include "memheap.h"

#include <assert.h>
#include <stddef.h>
#include <string.h>

#include <inttypes.h>
#include <stdio.h>

#include "memheap_internal.h"


uint32_t membuffer_hdr_compute_checksum(membuffer_hdr_t* hdr) {
    uint64_t checksum = 0;
    checksum ^= hdr->size ^ hdr->offset_previous ^ hdr->offset_next;
    uint32_t cs_u32 = (checksum >> 32) ^ checksum ^ hdr->allocated;
    return cs_u32;
}

void memheap_setbuffer_hdr(
    void* addr, size_t offset,
    uint64_t size,
    int64_t offset_previous, int64_t offset_next,
    uint32_t allocated
) {
    membuffer_hdr_t hdr = {
        .size = size, .offset_previous = offset_previous,
        .offset_next = offset_next, .allocated = allocated
    };
    hdr.checksum = membuffer_hdr_compute_checksum(&hdr);
    intptr_t hdr_addr = (intptr_t)addr + offset;
    memcpy((void*) hdr_addr, &hdr, sizeof(membuffer_hdr_t));
}

void memheap_init(memheap_t* heap, void* offset, size_t size)
{
    heap->addr_start = offset;
    heap->size = size;
    memheap_setbuffer_hdr(
        heap->addr_start,
        0,
        size - sizeof(membuffer_hdr_t),
        0, 0, 0
    );
}


/** return the address to the hdr of a memory buffer object from its offset
 * @param heap memory allocation heap structure
 * @param offset hdr offset within heap data
 * @return hdr address
 */
static membuffer_hdr_t* get_hdr_addr(memheap_t* heap, uint64_t offset)
{
    intptr_t hdr_addr = (intptr_t)heap->addr_start + offset;
    return (membuffer_hdr_t*) hdr_addr;
}

void* memheap_malloc(memheap_t* heap, size_t size) {
    // aligning size on sizeof(membuffer_hdr_t)
    size = (size + sizeof(membuffer_hdr_t) - 1) / sizeof(membuffer_hdr_t);
    size *= sizeof(membuffer_hdr_t);
    // looking for first non-allocated buffer big enough
    uint64_t offset = 0;
    membuffer_hdr_t* hdr = (membuffer_hdr_t*)heap->addr_start + offset;
    while ((hdr->allocated || hdr->size < size) && hdr->offset_next) {
        assert(hdr->offset_next < heap->size);
        offset = hdr->offset_next;
        hdr = get_hdr_addr(heap, offset);
    }

    if (hdr->allocated || hdr->size < size) {
        return NULL;
    }
    // buffer is big enough for data, not for splitting
    if (hdr->size - size <= sizeof(membuffer_hdr_t)) {
        memheap_setbuffer_hdr(
            heap->addr_start, offset, hdr->size,
            hdr->offset_previous, hdr->offset_next,
            1);
    } else {
        // splitting buffer
        uint64_t offset_next = hdr->offset_next;
        uint64_t offset_free = offset + size + sizeof(membuffer_hdr_t);
        uint64_t free_size = hdr->size - (size + sizeof(membuffer_hdr_t));
        // setting hdr buffer
        memheap_setbuffer_hdr(
            heap->addr_start, offset, size,
            hdr->offset_previous, offset_free,
            1);
        // setting remaing free buffer hdr
        assert(free_size > 0);
        memheap_setbuffer_hdr(
            heap->addr_start, offset_free, free_size,
            offset, offset_next,
            0);
        // setting next buffer hdr
        if (offset_next) {
            membuffer_hdr_t* hdr_next = get_hdr_addr(heap, offset_next);
            memheap_setbuffer_hdr(
                heap->addr_start, offset_next, hdr_next->size,
                hdr_next->offset_previous, hdr_next->offset_next,
                hdr_next->allocated);
        }
    }

   return (void*)((intptr_t)heap->addr_start + offset + sizeof(membuffer_hdr_t));
}

void* memheap_calloc(memheap_t* heap, size_t nmemb, size_t size)
{
    void* addr = memheap_malloc(heap, nmemb * size);
    if (addr) memset(addr, 0, nmemb * size);
    return addr;

}

static void memheap_fix_hdr_previous_offset(
    memheap_t* heap, uint64_t offset, uint64_t previous_offset)
{
    membuffer_hdr_t* hdr = get_hdr_addr(heap, offset);
    memheap_setbuffer_hdr(
        heap->addr_start, offset, hdr->size,
        previous_offset, hdr->offset_next,
        hdr->allocated);
}

void memheap_free(memheap_t* heap, void* addr)
{
    intptr_t hdr_addr = (intptr_t) addr - sizeof(membuffer_hdr_t);
    ptrdiff_t offset = hdr_addr - (intptr_t)heap->addr_start;
    assert(offset >= 0);
    membuffer_hdr_t* hdr = (membuffer_hdr_t*) hdr_addr;
    if (hdr->offset_next) {
        membuffer_hdr_t* next_hdr = get_hdr_addr(heap, hdr->offset_next);
        if (!next_hdr->allocated) {
            // merging with next free buffer
            memheap_setbuffer_hdr(
                heap->addr_start, offset,
                hdr->size + next_hdr->size + sizeof(membuffer_hdr_t),
                hdr->offset_previous, next_hdr->offset_next,
                0);
            if (hdr->offset_next) {
                memheap_fix_hdr_previous_offset(heap, hdr->offset_next, offset);
            }
        }
    }
    // if buffer is not the first
    if (offset) {
        // trying to merge buffer with previous
        membuffer_hdr_t* previous_hdr = get_hdr_addr(heap, hdr->offset_previous);
        if (!previous_hdr->allocated) {
            offset = hdr->offset_previous;
            memheap_setbuffer_hdr(
                heap->addr_start, offset,
                hdr->size + previous_hdr->size + sizeof(membuffer_hdr_t),
                previous_hdr->offset_previous, hdr->offset_next,
                0);
            if (hdr->offset_next) {
                memheap_fix_hdr_previous_offset(heap, previous_hdr->offset_next, offset);
            }
            hdr = previous_hdr;
        }
    }
    memheap_setbuffer_hdr(
        heap->addr_start, offset, hdr->size,
        hdr->offset_previous, hdr->offset_next,
        0);
}

void memheap_display_info(memheap_t* heap) {
    uint64_t size = heap->size, allocated_size = 0, free_size = 0;
    uint64_t offset = 0;
    membuffer_hdr_t* hdr = (membuffer_hdr_t*)heap->addr_start + offset;
    if (hdr->allocated) allocated_size += hdr->size;
    else free_size += hdr->size;
    while (hdr->offset_next) {
        assert(hdr->offset_next < heap->size);
        offset = hdr->offset_next;
        hdr = get_hdr_addr(heap, offset);
        if (hdr->allocated) allocated_size += hdr->size;
        else free_size += hdr->size;
    }
    uint64_t meta_size = size - (allocated_size + free_size);
    printf("heap size       =%"PRIu64"\n", size);
    printf("  allocated size=%"PRIu64"\n", allocated_size);
    printf("  free size     =%"PRIu64"\n", free_size);
    printf("  meta_size     =%"PRIu64"/%.3f%%\n", meta_size, meta_size / (double) size * 100.0);
}
