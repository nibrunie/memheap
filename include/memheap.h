#pragma once

#include <stdint.h>
#include <stddef.h>

/** Memory heap structure */
typedef struct {
    /** start address of data buffer */
    void* addr_start;
    /** size of data buffer */
    uint64_t size;
} memheap_t;

/** Initialized memory heap structure
 *  @param[out] heap structure to initialize
 *  @param offset start address for data buffer
 *  @param size allocated data buffer size
 */
void memheap_init(memheap_t* heap, void* offset, size_t size);

/** memory heap malloc
 *  @param heap memory heap context
 *  @param size number of byte(s) to be allocated
 *  @return pointer of the buffer newly allocated
 */
void* memheap_malloc(memheap_t* heap, size_t size);

/** memory heap calloc (malloc + zeroification)
 *  @param heap memory heap context
 *  @param size number of byte(s) to be allocated
 *  @return pointer of the buffer newly allocated
 */
void* memheap_calloc(memheap_t* heap, size_t nmemb, size_t size);

/** memory heap buffer free
 *  @param heap memory heap context
 *  @param addr address of the buffer to free
 */
void memheap_free(memheap_t* heap, void* addr);

/** Display information of memory heap structure
 *  @param heap memory heap context
 */
void memheap_display_info(memheap_t* heap);
