#include <stdint.h>

/** memory buffer header structure */
typedef struct {
    /** local buffer size */
    uint64_t size;
    /** offset of previsou buffer */
    int64_t offset_previous;
    /** offset of next buffer */
    int64_t offset_next;
    /** is the buffer allocated */
    uint32_t allocated;
    /** buffer header checksum */
    uint32_t checksum;
} membuffer_hdr_t;
