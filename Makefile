CFLAGS += -Wall -Werror --pedantic

memheap.o: src/memheap.c
	$(CC) -c -Iinclude $(CFLAGS) -o $@ $^

./test_memheap: memheap.o tests/test_memheap.c
	$(CC) -Iinclude $(CFLAGS) -o $@ $^

test: ./test_memheap
	./test_memheap
