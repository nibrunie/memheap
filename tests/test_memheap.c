#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "memheap.h"
#include "memheap_internal.h"



int main(void) {
    const size_t GLOBAL_SIZE = 100000000; // 100 MB
    void* global_buffer = malloc(GLOBAL_SIZE);
    const int verbose = 0;

    memheap_t* heap = calloc(1, sizeof(memheap_t));

    printf("initializing kaf mem heap structure\n");
    memheap_init(heap, global_buffer, GLOBAL_SIZE);

    printf("starting alloc / free sequence \n");
    const int ALLOC_NUM = 2048;
    for (int i = 0; i < ALLOC_NUM; ++i)
    {
        size_t size = 0;
        while (!size) size = (rand() % 256) * 17;
        void* buffer = memheap_malloc(heap, size);
        if (!buffer) {
            printf("failed to allocate buffer %d of size %zu\n", i, size);
            return -1;
        }
        memset(buffer, 0x2a, size);
        if (verbose) printf("buffer of size %zu allocated @%p\n", size, buffer);
        memheap_free(heap, buffer);
    }
    printf("  test OK\n");
    memheap_display_info(heap);


    printf("starting multi alloc / multi free sequence \n");
    const int TEST_NUM = 10;
    for (int j = 0; j < TEST_NUM; ++j) {
        const int ALLOC_NUM = 2048;
        void** buffer_addr_v = calloc(sizeof(void*), ALLOC_NUM);
        size_t* buffer_size_v = calloc(sizeof(size_t), ALLOC_NUM);
        for (int i = 0; i < ALLOC_NUM; ++i)
        {
            size_t size = 0;
            while (!size) size = (rand() % 256) * 17;
            void* buffer = memheap_malloc(heap, size);
            if (!buffer) {
                printf("failed to allocate buffer %d of size %zu\n", i, size);
                return -1;
            }
            if (verbose) printf("buffer of size %zu allocated @%p\n", size, buffer);

            buffer_addr_v[i] = buffer;
            buffer_size_v[i] = size;
        }
        for (int i = 0; i < ALLOC_NUM; ++i)
        {
            memset(buffer_addr_v[i], 0x2a, buffer_size_v[i]);
        }
        if (verbose) printf("  trying to free %d allocated buffer(s)\n", ALLOC_NUM);
        for (int i = 0; i < ALLOC_NUM; ++i)
        {
            memheap_free(heap, buffer_addr_v[i]);
        }
        free(buffer_addr_v);
        free(buffer_size_v);
    }
    memheap_display_info(heap);
    printf("  test OK\n");
    size_t max_size = GLOBAL_SIZE - sizeof(membuffer_hdr_t);
    printf("testing max size alloc %zu B\n", max_size);
    for (int i = 0; i < ALLOC_NUM; ++i)
    {
        size_t size = max_size;
        void* buffer = memheap_malloc(heap, size);
        if (!buffer) {
            printf("failed to allocate buffer %d of size %zu\n", i, size);
            return -1;
        }
        memset(buffer, 0x2a, size);
        if (verbose) printf("buffer of size %zu allocated @%p\n", size, buffer);
        memheap_free(heap, buffer);
    }
    memheap_display_info(heap);
    printf("  test OK\n");
    size_t min_size = 0; // GLOBAL_SIZE - sizeof(membuffer_header_t);
    printf("testing min size alloc %zu B\n", min_size);
    for (int i = 0; i < ALLOC_NUM; ++i)
    {
        size_t size = min_size;
        void* buffer = memheap_malloc(heap, size);
        if (!buffer) {
            printf("failed to allocate buffer %d of size %zu\n", i, size);
            return -1;
        }
        memset(buffer, 0x2a, size);
        if (verbose) printf("buffer of size %zu allocated @%p\n", size, buffer);
        memheap_free(heap, buffer);
    }
    memheap_display_info(heap);
    printf("  test OK\n");

    free(global_buffer);

    return 0;

}
